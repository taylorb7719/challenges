import csv
import operator
from collections import Counter

with open('green.csv') as csv_file:
    next(csv_file)
    next(csv_file)
    Agg = {}
    csv_reader = csv.reader(csv_file, delimiter=',')
    counter = 0
    for row in csv_reader:
        puTime = row[1]
        puTimeParsed = puTime.split(' ')
        puTimeTemp = puTimeParsed[-1]
        puTimeParsed = puTimeTemp.split(':')
        puTimeParsed = puTimeParsed[0]

        puLoc = row[5]
        try:
            if len(Agg[puTimeParsed]) > 0:
                try:
                    if Agg[puTimeParsed][puLoc] > 0:
                        count = Agg[puTimeParsed][puLoc]
                        Agg[puTimeParsed][puLoc] = count+1
                except KeyError:
                    Agg[puTimeParsed][puLoc] = 1
        except KeyError:
            Agg[puTimeParsed] = {}
            Agg[puTimeParsed][puLoc] = 1
    for x in Agg.keys():
        print x
        y = Counter(Agg[x])
        y.most_common()
        for k, v in y.most_common(3):
             print '%s: %i' % (k, v)


    # print Agg["01"]
    # for k,v in Agg:
    #     print k
    # print Agg

        # DO time
        # print(row[2])
        # # DO Loc
        # print(row[6])
