import csv
import operator
from collections import Counter

MegaPU = {}
MegaDO = {}

def main():
    zones = getZones()
    print("Green PU")
    parseFilePU("green.csv", 1, 5)
    print("Green DO")
    parseFileDO("green.csv", 2, 6)
    print("Yellow PU")
    parseFilePU("yellow.csv", 1, 7)
    print("Yellow DO")
    parseFileDO("yellow.csv", 2, 8)
    print("fhv PU")
    parseFilePU("fhv_tripdata_2018-01.csv", 0, 2)
    print("fhv DO")
    parseFileDO("fhv_tripdata_2018-01.csv", 1, 3)

    print("Pickup")
    for x in MegaPU.keys():
        print("Hour: %s" % x)
        MegaPU[x].pop('', None)
        MegaPU[x].pop('265', None)
        y = Counter(MegaPU[x])
        y.most_common()
        for k, v in y.most_common(3):
            print '%s: %i' % (zones[int(k)-1], v)


    print("\n\n\n")

    print("Dropoff")
    # print(MegaDO)
    for x in MegaDO.keys():
        print("Hour: %s" % x)
        MegaDO[x].pop('', None)
        MegaDO[x].pop('265', None)
        y = Counter(MegaDO[x])
        y.most_common()
        for k, v in y.most_common(3):
            try:
                print '%s: %i' % (zones[int(k)-1], v)
            except IndexError:
                print '%s: %i' % (k, v)




def getZones():
    print("Get Zones")
    filename = "taxi_zone_lookup.csv"
    zones = []
    with open(filename) as csv_file:
        next(csv_file)
        csv_reader = csv.reader(csv_file, delimiter=',')
        counter = 0
        for row in csv_reader:
            zones.append(row[2])
    return zones


def parseFilePU(filename, timeIndex, locIndex):
    with open(filename) as csv_file:
        next(csv_file)
        next(csv_file)
        csv_reader = csv.reader(csv_file, delimiter=',')
        counter = 0
        for row in csv_reader:
            puTime = row[timeIndex]
            puTimeParsed = puTime.split(' ')
            puTimeTemp = puTimeParsed[-1]
            puTimeParsed = puTimeTemp.split(':')
            puTimeParsed = puTimeParsed[0]

            puLoc = row[locIndex]
            try:
                if len(MegaPU[puTimeParsed]) > 0:
                    try:
                        if MegaPU[puTimeParsed][puLoc] > 0:
                            count = MegaPU[puTimeParsed][puLoc]
                            MegaPU[puTimeParsed][puLoc] = count+1
                    except KeyError:
                        MegaPU[puTimeParsed][puLoc] = 1
            except KeyError:
                MegaPU[puTimeParsed] = {}
                MegaPU[puTimeParsed][puLoc] = 1
        # for x in MegaPU.keys():
        #     print("Hour: %s" % x)
        #     y = Counter(MegaPU[x])
        #     y.most_common()
        #     for k, v in y.most_common(3):
        #         print '%s: %i' % (k, v)

def parseFileDO(filename, timeIndex, locIndex):
    with open(filename) as csv_file:
        next(csv_file)
        next(csv_file)
        csv_reader = csv.reader(csv_file, delimiter=',')
        counter = 0
        for row in csv_reader:
            puTime = row[timeIndex]
            puTimeParsed = puTime.split(' ')
            puTimeTemp = puTimeParsed[-1]
            puTimeParsed = puTimeTemp.split(':')
            puTimeParsed = puTimeParsed[0]

            puLoc = row[locIndex]
            try:
                if len(MegaDO[puTimeParsed]) > 0:
                    try:
                        if MegaDO[puTimeParsed][puLoc] > 0:
                            count = MegaDO[puTimeParsed][puLoc]
                            MegaDO[puTimeParsed][puLoc] = count+1
                    except KeyError:
                        MegaDO[puTimeParsed][puLoc] = 1
            except KeyError:
                MegaDO[puTimeParsed] = {}
                MegaDO[puTimeParsed][puLoc] = 1
        # for x in MegaDO.keys():
        #     print("Hour: %s" % x)
        #     y = Counter(MegaDO[x])
        #     y.most_common()
        #     for k, v in y.most_common(3):
        #         print '%s: %i' % (k, v)

if __name__=="__main__":
    main()
