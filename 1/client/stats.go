package main

import "fmt"
import "strings"
import "os"
import "bufio"
import "regexp"
import "strconv"
import "net/http"

func main() {
	// Gathers random system stats
	http.HandleFunc("/", stats)

	err := http.ListenAndServe(":9092", nil)
	if err != nil {
		fmt.Println("ListenAndServe: ", err)
	}
}

func memoryStats() {
	var memTotal string
	var memAvailable string

	filename := "/proc/meminfo"
	file, err := os.Open(filename)
	if err != nil {
		fmt.Println("Proc was not found, trying local...")
		filename = "./meminfo"
		file, err = os.Open(filename)
		if err != nil {
			panic(err)
		}
	}
	scanner := bufio.NewScanner(file)
	re := regexp.MustCompile("[0-9]+")
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "MemTotal") {
			regexResult := re.FindAllString(line, 1)
			memTotal = regexResult[0]
		} else if strings.HasPrefix(line, "MemAvailable") {
			regexResult := re.FindAllString(line, 1)
			memAvailable = regexResult[0]
		}
	}
	file.Close()

	memTotalInt, _ := strconv.Atoi(memTotal)
	memAvailableInt, _ := strconv.Atoi(memAvailable)
	memUsedFloat := float64(memTotalInt - memAvailableInt)
	memPercentageUsedFloat := memUsedFloat / float64(memTotalInt)
	percentageFormat := fmt.Sprintf("%.2f", memPercentageUsedFloat)

	fmt.Println(memTotal)
	fmt.Println(memAvailable)
	fmt.Println(memUsedFloat)
	fmt.Println(percentageFormat)
}

func cpuStats() string {
	var cpu1min string
	var cpu5min string
	var cpu15min string

	filename := "/proc/loadavg"

	file, err := os.Open(filename)
	if err != nil {
		fmt.Println("Proc was not found, trying local...")
		filename = "./loadavg"
		file, err = os.Open(filename)
		if err != nil {
			panic(err)
		}
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		items := strings.Split(line, " ")
		cpu1min = items[0]
		cpu5min = items[1]
		cpu15min = items[2]
	}

	fmt.Println(cpu1min)
	fmt.Println(cpu5min)
	fmt.Println(cpu15min)
	return cpu1min

}

func stats(w http.ResponseWriter, r *http.Request) {
	msg := cpuStats()
	fmt.Fprintf(w, msg)
}
